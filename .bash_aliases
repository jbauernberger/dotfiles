#    _               _              _ _                     
#   | |__   __ _ ___| |__      __ _| (_) __ _ ___  ___  ___ 
#   | '_ \ / _` / __| '_ \    / _` | | |/ _` / __|/ _ \/ __|
#  _| |_) | (_| \__ \ | | |  | (_| | | | (_| \__ \  __/\__ \
# (_)_.__/ \__,_|___/_| |_|___\__,_|_|_|\__,_|___/\___||___/
#                        |_____|                            
# ~/.bash_aliases → sourced from ~/.bashrc

alias lw="libreoffice --writer"
alias lc="libreoffice --calc"
alias pig="ping www.google.it"
alias lmutt="neomutt -F ~/.muttrc_sys"
alias vamutt="neomutt -F ~/.muttrc"
alias samutt="neomutt -F ~/.muttrc_sa"
alias pgrep="pgrep -f"
alias svi="sudoedit"
alias eip="curl http://icanhazip.com/"
alias ccat="highlight --out-format=ansi"
alias bcat="batcat"
alias yt="youtube-dl --add-metadata -ic"
alias yta="youtube-dl --add-metadata -xic"
alias wspeed="speedometer -r wlp2s0"
alias rg="ripgrep"

# show keys on X11/i3
alias screenkey="screenkey -f 'DejaVu Sans Mono Book 22' -p bottom --bg-color '#2f343f' --font-color '#f3f4f5'"

# show keys on Wayland/sway
# https://git.sr.ht/~sircmpwn/wshowkeys
alias wshowkeys="wshowkeys -b '#458588CC' -f '#EBDBB2' -s '#FFFFFF' -F 'Dejavu Sans Mono 32' -a bottom -m 80 -t 1"

# manage dotfiles with git (https://www.atlassian.com/git/tutorials/dotfiles)
alias config='/usr/bin/git --git-dir=/home/joachim/.dotfiles/ --work-tree=/home/joachim'

alias what="sudo /home/joachim/.cargo/bin/bandwhich"

alias vaio="ssh joachim@vaio"
#alias imv="/usr/libexec/imv/imv"

alias transmission-gtk="GDK_BACKEND=x11 transmission-gtk"
alias aptu="sudo apt update && apt list --upgradeable"
alias python="python3"
alias which="command -v"
alias lg='lazygit'

alias fx="/usr/bin/firefox --new-instance --profile $(mktemp -d)"
