## common functions available to other scripts
##
function certchain() {
    # Usage: certchain
    # Display PKI chain-of-trust for a given domain
    # GistID: https://gist.github.com/joshenders/cda916797665de69ebcd
    if [[ "$#" -ne 1 ]]; then
        echo "Usage: ${FUNCNAME} <ip|domain[:port]>"
        return 1
    fi

    local host_port="$1"

    if [[ "$1" != *:* ]]; then
        local host_port="${1}:443"
    fi

    openssl s_client -connect "${host_port}" </dev/null 2>/dev/null | grep -E '\ (s|i):' && return ${PIPESTATUS[0]}
}
export -f certchain

## pulse default sinks

function get_pulse_default_sink() {
    pacmd info | grep 'Default sink' | awk '{print $4}' && return ${PIPESTATUS[0]}
}

function get_pulse_sink_number() {
    pacmd info | grep "sink:.*$sinkName" | awk '{print $2}' && return ${PIPESTATUS[0]}
}

## unfortunatly edid-decode hangs sometimes and parsing /sys pci values is
## brittle. use xrandr instead:

function get_monitor_screen_resolution() {
    xrandr | grep -w connected  | awk -F'[ +]' '{print $4}' && return ${PIPESTATUS[0]}
}
export -f get_monitor_screen_resolution

function get_monitor_name() {
    xrandr | grep -w connected  | awk -F'[ +]' '{print $1}' && return ${PIPESTATUS[0]}
}
export -f get_monitor_name

function get_monitors() {
    xrandr | grep -w connected  | awk -F'[ +]' '{print $1,$3,$4}' && return ${PIPESTATUS[0]}
}
export -f get_monitors

# show_color() print color to terminal from hexcode
# expects either a filename as single argument, or
# a list of hex codes: 'dddddd d3d3d3' (without #)
function show_color() {
  local colors=
  if [ $# -eq 1 ]; then
    if [ -r "$1" ]; then
      colors=`grep -oE '#[0-9a-fA-F]{6}+' "$1" | sed -re 's/^\#//g'| sort -u|uniq -i|xargs -rd'\n'`
    else
      colors="$1"
    fi
  else
    colors="$*"
  fi
  if [ -z "$colors" ]; then
    return
  fi
  declare -a "a_colors=($colors)"
  perl -e 'foreach $a(@ARGV){print $a." = "; for (my $i=0;$i<79;$i++){print "\e[48;2;".join(";",unpack("C*",pack("H*",$a)))."m \e[49m"} print "\n"};print "\n"' "${a_colors[@]}"
}
export -f show_color

function mkcdir () { 
  mkdir -- "$1" && cd -- "$1"; 
}
export -f mkcdir

## some ideas below were taken from
## https://github.com/dylanaraps/pure-bash-bible#strings
function xregex() {
	# Usage: regex "string" "regex"
	[[ $1 =~ $2 ]] && printf '%s\n' "${BASH_REMATCH[1]}"
}
export -f xregex

function xsplit() {
	# Usage: split "string" "delimiter" field
  # field: represents the element in the array starting with 0
	IFS=$'\n' read -d "" -ra arr <<< "${1//$2/$'\n'}"
	if [ -z "$3" ]; then
		printf '%s\n' "${arr[@]}"
	else
		printf '%s\n' "${arr[$3]}"
	fi
}
export -f xsplit

function cheat() {
  curl cht.sh/$1
}
export -f cheat
