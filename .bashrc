#    _               _              
#   | |__   __ _ ___| |__  _ __ ___ 
#   | '_ \ / _` / __| '_ \| '__/ __|
#  _| |_) | (_| \__ \ | | | | | (__ 
# (_)_.__/ \__,_|___/_| |_|_|  \___|
#
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=-1
HISTFILESIZE=-1

# After each command, append to the history file and reread it
export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize autocd

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Get color support for 'less'
export LESSOPEN='|~/.lessfilter %s'

# interpret color characters
export LESS='-R'

# to list available styles: `pygmentize -L styles`
export PYGMENTIZE_STYLE='dracula'

# colorful manpages
man() {
    LESS_TERMCAP_mb=$'\e'"[0;32m" \
    LESS_TERMCAP_md=$'\e'"[0;32m" \
    LESS_TERMCAP_me=$'\e'"[0m" \
    LESS_TERMCAP_se=$'\e'"[0m" \
    LESS_TERMCAP_so=$'\e'"[1;40;44m" \
    LESS_TERMCAP_ue=$'\e'"[0m" \
    LESS_TERMCAP_us=$'\e'"[0;32m" \
    command man "$@"
}

#export GROFF_NO_SGR=1                  # for konsole and gnome-terminal

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color|xterm-kitty) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

export PROMPT_COMMAND=__prompt_command  # Func to gen PS1 after CMDs

__prompt_command() {
	local EXIT="$?"             # This needs to be first
	PS1=""

  # see ~/bin/colortest:
	local RCol='\[\e[0m\]'
	local Red='\[\e[0;31m\]'
	local Gre='\[\e[0;32m\]'
	local BBlu='\[\e[0;34m\]'
	local Pur='\[\e[0;35m\]'

	if [ "$color_prompt" = yes ]; then
									
		if [ -z "$DISPLAY" ] && [ -z "$WAYLAND_DISPLAY"  ]; then # not in X

            if [ $EXIT != 0 ]; then
              PS1+="${Red}err:${EXIT}${RCol} " 
            else
                PS1+="${Gre}ok${RCol} "
            fi

            # display hostname for remote clients:
            if [ -z "$SSH_TTY" ]; then
		        PS1+="\u${BBlu}\w${RCol}\n${Gre}\$ ${RCol}"
            else
                # ssh:
		        PS1+="\u${BBlu}@\h${RCol} ${BBlu}\w${RCol}\n${Gre}\$ ${RCol}"
            fi

		else

            if [ $EXIT != 0 ]; then
                PS1+="${Red} ($EXIT)${RCol} " 
            else
                PS1+="${Gre}${RCol} "
            fi

			PS1+="\u ${BBlu}\w${RCol}\n${Pur} ${RCol}"

		fi
	   
	else
		PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
	fi
}

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  eval "$(dircolors "$HOME/.config/dir_colors/dracula")"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
alias la='ls -al'
alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

if [ -f ~/.bash_functions ]; then
    . ~/.bash_functions
fi

# make (i-search) work:
stty -ixon

# Searching through history. In the terminal, pressing the up arrow 
# cycles through previous commands you used starting with the most recent 
# first
bind '"\e[A":history-search-backward'; bind '"\e[B":history-search-forward'

export EDITOR=vim
export PAGER=less

# set up PATH and other things specific to this host
host=$(/bin/hostname)
if [ -f "$HOME/.bash_${host}" ]; then
    . "$HOME/.bash_${host}"
fi

# X11 only:
# - sway has session_type "tty" so dpms is set in sway/config instead
# XDG_SESSION_TYPE gets set in .xinitrc (for startx)
if [ "$XDG_SESSION_TYPE" == "x11" ]; then
    ## set screen blanking to 1 hour
    xset dpms 0 0 3600
    xset s 3600 3600
    declare -rx CONNECTED_MONITOR=$(get_monitor_name)
    declare -rx CONNECTED_MONITOR_RES=$(get_monitor_screen_resolution)
fi
. "$HOME/.cargo/env"
