#
#  _ _____                    __ _       
# (_)___ /    ___ ___  _ __  / _(_) __ _ 
# | | |_ \   / __/ _ \| '_ \| |_| |/ _` |
# | |___) | | (_| (_) | | | |  _| | (_| |
# |_|____/   \___\___/|_| |_|_| |_|\__, |
#                                  |___/ 
#
# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

# xrandr | grep -w connected  | awk -F'[ +]' '{print $1}'
set $monitor "eDP1"
# xrandr | grep -w connected  | awk -F'[ +]' '{print $4}'
set $resolution "1920x1080"
# pacmd info | grep 'Default sink' | awk '{print $4}'
set $pasink alsa_output.pci-0000_00_1f.3.analog-stereo

set $mod Mod4

# Font for window titles. Will also be used by the bar
font pango:VictorMono 11,FontAwesome 11

# TODO: set these in Xresources and fetch via "set_from_resource"
set $bg-color 	         #353945
set $inactive-bg-color   #404552
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #bd2c40
set $indicator-color     #404552

gaps inner 8
gaps outer 5
smart_gaps on

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# swith to urgent workspace:
bindsym $mod+u [urgent=latest] focus

# start a terminal
bindsym $mod+Return exec alacritty

# kill focused window
bindsym $mod+Shift+q kill

# use rofi instead of dmenu 
bindsym $mod+d exec rofi -combi-modi "run#ssh" -show combi -hide-scrollbar -lines 8 -eh 1 -width 30 -padding 20 -bw 2 -font "DejaVu Sans Mono Bold 14"

bindsym $mod+Shift+D exec mimeopen -n "$(locate -e -i --regex "/home/$USER/[^\.]+" | rofi -location 0 -threads 0 -show-combi -dmenu -i -p "text_or_icon")"

# Printscreen to grab the full screen
bindsym --release Print exec scrot -q 100 -u '%Y-%m-%d_%H%M%S_$wx$h_infocus_scrot.png' -e 'mv $f ~/Shots/'

# Printscreen calls scrot and selects the window/container which is in focus
bindsym --release $mod+Print exec scrot -q 100 '%Y-%m-%d_%H%M%S_$wx$h_rootwin_scrot.png' -e 'mv $f ~/Shots/'

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+odiaeresis focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+odiaeresis move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

hide_edge_borders both
# set size of border
for_window [class=".*"] border pixel 1

# Hide titlebar
new_window pixel

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 1: 
set $ws2 2: 
set $ws3 3: 
set $ws4 4: 
set $ws5 5: 
set $ws6 6: 
set $ws7 7: 
set $ws8 8:
set $ws9 9:
set $ws10 10:

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# exit i3 
set $i3exit /home/joachim/.config/i3/scripts/i3exit
bindsym $mod+Shift+x exec --no-startup-id xautolock -locknow -nowlocker "$i3exit lock"

set $mode_system System (l) lock, (e) logout, (r) reboot, (Shift+s) shutdown
mode "$mode_system" {
    bindsym l exec --no-startup-id $i3exit lock, mode "default"
    bindsym e exec --no-startup-id $i3exit logout, mode "default"
    #bindsym s exec --no-startup-id $i3exit suspend, mode "default"
    #bindsym h exec --no-startup-id $i3exit hibernate, mode "default"
    bindsym r exec --no-startup-id $i3exit reboot, mode "default"
    bindsym Shift+s exec --no-startup-id $i3exit shutdown, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Pause mode "$mode_system"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym odiaeresis resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# window colors
#                       border              background         text                indicator
#client.focused          $bg-color          $bg-color          $text-color          $indicator-color
#client.unfocused        $inactive-bg-color $inactive-bg-color $inactive-text-color $indicator-color
#client.focused_inactive $inactive-bg-color $inactive-bg-color $inactive-text-color $indicator-color
#client.urgent           $urgent-bg-color   $urgent-bg-color   $text-color          $indicator-color

# dracula:
client.focused          #6272A4 #6272A4 #F8F8F2 #6272A4   #6272A4
client.focused_inactive #44475A #44475A #F8F8F2 #44475A   #44475A
client.unfocused        #282A36 #282A36 #BFBFBF #282A36   #282A36
client.urgent           #44475A #FF5555 #F8F8F2 #FF5555   #FF5555
client.placeholder      #282A36 #282A36 #F8F8F2 #282A36   #282A36
client.background       #F8F8F2

# Pulse Audio controls
# using numeric sink id format might interfere when pavucontrol & friends
# modify the default sink. to get the name check with "pactl list sinks"

bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume $pasink +5% #increase volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume $pasink -5% #decrease volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute $pasink toggle # mute sound

bindsym XF86MonBrightnessUp exec brightnessctl -d intel_backlight s +10% # increase screen brightness
bindsym XF86MonBrightnessDown exec brightnessctl -d intel_backlight s 10%- # decrease screen brightness

# Media player controls
bindsym $mod+t exec mpc toggle
bindsym $mod+q exec mpc stop
bindsym $mod+n exec mpc next
bindsym $mod+p exec mpc prev
bindsym $mod+x exec mpc stop

# workspace 5 reserved for GIMP
for_window [title="GIMP Startup"] move workspace $ws5
for_window [class="Gimp"] move workspace $ws5
#for_window [window_role="gimp-dock"] floating enable; move left; resize shrink width 50 px or 50ppt
#for_window [window_role="gimp-toolbox"] floating enable; move right; resize grow width 30 px or 30ppt

# workspace 6 for chat applications
for_window [title="ricochet"] move workspace $ws6
for_window [class="Signal"] move workspace $ws6
for_window [title="Skype"] move workspace $ws6

###
# autostart applications:
#exec_always --no-startup-id pulseaudio --start
exec_always --no-startup-id xrandr --output $monitor --mode $resolution
#exec_always --no-startup-id pulseeffects --gapplication-service
exec_always --no-startup-id xautolock -corners +00- -time 60 -locker "$i3exit lock"
exec_always --no-startup-id feh --bg-scale --randomize ~/Pictures/wallpapers/*.png
exec_always --no-startup-id unclutter

exec --no-startup-id dunst -conf ~/.config/dunst/dunstrc
exec ~/.config/i3/scripts/i3-battery-popup -m "Low Battery" -t 1m -i /usr/share/icons/gnome/16x16/status/battery-caution.png -n -L 14 -s ~/src/i3-battery-popup/i3-battery-popup.wav
#exec --no-startup-id ~/.config/i3/scripts/mpd_start.sh

exec --no-startup-id picom -b --blur-background


## Workspace 1:
exec --no-startup-id i3-msg 'workspace $ws1; append_layout ~/.config/i3/workspace-1.json'
exec --no-startup-id i3-msg 'workspace $ws1; exec i3-sensible-terminal'
exec --no-startup-id i3-msg 'workspace $ws1; exec i3-sensible-terminal'
exec --no-startup-id i3-msg 'workspace $ws1; exec i3-sensible-terminal'

## Workspace 2:
exec --no-startup-id i3-msg 'workspace $ws2; exec firefox'
exec --no-startup-id i3-msg 'workspace $ws2; exec keepassxc'

## Workspace 3:
#exec --no-startup-id i3-msg 'workspace $ws3; exec zathura'

## Workspace 4:
#exec --no-startup-id i3-msg 'workspace $ws4; exec thunar'

# starting polybar too soon results in a race condition where either mpd or
# pulseaudio aren't yet available
exec_always --no-startup-id $HOME/.config/polybar/launch.sh
