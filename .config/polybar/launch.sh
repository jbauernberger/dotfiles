#!/usr/bin/env bash

if [ -f ~/.bash_functions ]; then
    . ~/.bash_functions
fi

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
CONNECTED_MONITOR=$(get_monitor_name) polybar tinkerbell &

echo "Polybar launched..."
