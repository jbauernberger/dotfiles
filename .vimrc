"        _                    
" __   _(_)_ __ ___  _ __ ___ 
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__ 
" (_)_/ |_|_| |_| |_|_|  \___|
"
" http://vi-improved.org/vimrc.html
       
set encoding=utf-8
execute pathogen#infect()
filetype on
filetype plugin on
filetype indent on
set background=dark
syntax enable
"set syntax=on
"set t_Co=256
"let g:gruvbox_transparent_bg=1 
"autocmd vimenter * colorscheme gruvbox
"colorscheme gruvbox

packadd! dracula
colorscheme dracula

" dracula options:
"let g:dracula_bold = 1
"let g:dracula_italic = 1
"let g:dracula_underline = 1
"let g:dracula_undercurl = 1
"let g:dracula_full_special_attrs_support = 1
"let g:dracula_inverse = 1
"let g:dracula_colorterm = 1
"
" dracula comment color is almost unreadable
" change it to the value used in gruvbox
:highlight Comment term=bold ctermfg=245 guifg=#928374

" ag - the silver searcher
let g:ackprg = 'ag --nogroup --nocolor --column'

" gitgutter: https://github.com/airblade/vim-gitgutter
set updatetime=100 " gitgutter
let g:gitgutter_max_signs = -1

runtime! macros/matchit.vim
set autoread        " allow undo with 'u'
set scrolloff=1
" search down into subfolders
" provides tab-completion for all file-related tasks
set path+=**
set nocompatible
set wildmenu        " display all matching files when we tab complete
set lisp            " adds '-' to the set of chars in a word
set autoindent
set smartindent
set cindent
set number
set hlsearch
set showmode
set ruler
set backspace=indent,eol,start " make backspace a more flexible
set backup          " make backup files
set backupdir=~/.vim/backup " where to put backup files
set directory=~/.vim/tmp " directory to place swap files in
set showcmd         " show the command being typed
set showmatch       " show matching brackets

" gitgutter:
function! GitStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a, m, r)
endfunction

set laststatus=2
set statusline=
set statusline+=%#DiffAdd#%{(mode()=='n')?'\ \ NORMAL\ ':''}
set statusline+=%#DiffChange#%{(mode()=='i')?'\ \ INSERT\ ':''}
set statusline+=%#DiffDelete#%{(mode()=='r')?'\ \ RPLACE\ ':''}
set statusline+=%#Cursor#%{(mode()=='v')?'\ \ VISUAL\ ':''}
set statusline+=\ %n\           " buffer number
set statusline+=%#Visual#       " colour
set statusline+=%{&paste?'\ PASTE\ ':''}
set statusline+=%{&spell?'\ SPELL\ ':''}
set statusline+=%#CursorIM#     " colour
set statusline+=%R                        " readonly flag
set statusline+=%M                        " modified [+] flag
set statusline+=%#Cursor#               " colour

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

set statusline+=%#CursorLine#     " colour
set statusline+=\ %t\                   " short file name
set statusline+=%=                          " right align
set statusline+=%#CursorLine#   " colour
set statusline+=\ %Y\                   " file type

set statusline+=%#CursorLine#     " colour
set statusline+=%{GitStatus()}
set statusline+=%#Cursor#       " colour

set statusline+=%#CursorIM#     " colour
set statusline+=\ %3l:%-2c\         " line + column
set statusline+=%#Cursor#       " colour
set statusline+=\ %3p%%\                " percentage

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_cpp_config_file = '.syntastic'
let g:syntastic_c_config_file = '.syntastic'

let g:syntastic_sh_shellcheck_args = "-x"

set expandtab       " no real tabs please!
set softtabstop=2   " how many spaces should a tab be (see expandtab)
set tabstop=2       " real tabs should be 4, and they will show with set list on
set shiftwidth=2    " auto-indent amount when using cindent, >>, << and stuff like that
set formatoptions=rq " Auto insert comment leader on return, and let gq format comments
set ignorecase      " case insensitive by default
set infercase       " case inferred by default
set wrap            " do wrap line
set smartcase       " if there are caps, go case-sensitive
set textwidth=79

au BufRead *.py set expandtab
" au BufRead *.c set noexpandtab
" au BufRead *.h set noexpandtab
au BufRead Makefile* set noexpandtab shiftwidth=2 softtabstop=0
au BufRead *.mk set noexpandtab shiftwidth=2 softtabstop=0


" spell check see: https://robots.thoughtbot.com/vim-spell-checking
au BufRead *.md setlocal spell
au BufRead *.txt setlocal spell
set complete+=kspell

autocmd bufreadpre *.md setlocal textwidth=0

" create tags file (requires ctags)
" use ^] to jump to tag under cursor
" use g^] for ambiguous tags
" use ^t to jump up the tag stack
command! MakeTags !ctags -R .

" tweaks for browsing
let g:netrw_banner=0        " disable banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_winsize = 25    " %25 of the window size
"let g:netrw_list_hide=netrw_gitignore#Hide()
"let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'


" NerdCommenter https://github.com/preservim/nerdcommenter
" Create default mappings
let g:NERDCreateDefaultMappings = 1
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1


" snippets:
" read a copyright stanca template
nnoremap ,copy :-1read $HOME/.vim/snippets/copyright.txt<CR>

" change builtin make to run something else
" See https://youtu.be/XA2WjJbmmoM?t=3409 for a rundown
" this allows you to run :make to run RSpec, and
" :cl to list errors
" :cc# to jump to error by number
" :cn and :cp to navigate to next and previous
"
" example:
" makeprg=bundle\ exec \ rspec\ -f\ QuickFormatter

" diagraph
dig :(  9785    " ☹  White Frowning Face 
dig :)  9786    " ☺  White Smiling Face
dig bh  9829    " ♥  Black Heart Suit
dig wh  9825    " ♥  White Heart Suit
dig oq  8223    " ‟  Opening Quote
dig cq  8221    " ”  Closing Quote
dig bs  9733    " ★  Black Star
dig ws  9734    " ☆  White Star
dig su 9788     " ☼  White Sun with Rays
dig mu 9835     " ♫  Beamed Eight Notes
dig pd  163     " £  Pound Sign
dig co  169     " ©  Copyright Sign
