#!/bin/sh

# backup script, run manually

DST_root=/media/$USER/Media/
DST="$DST_root/$(hostname)"
targets_root="/etc"
targets_user_delete="Memes Downloads .offlineimap* .config .cert .ssh .Mail Books Projects .cargo"
targets_user_data="$HOME/Videos $HOME/Music"
targets_user="Documents Pictures Notes texmf .config .fonts .dotfiles bin .mutt* .gnupg .ssh .vim* .bash* .profile .abook .signature .gitconfig .Xresources* .gtkrc-* .icons"

if [ -z "$(command -v rsync)" ]; then
	echo "$0: rsync command not on PATH ($PATH)" && exit 1
fi

if [ -d "$DST_root" ]; then
	mkdir -p "$DST" || ( echo "$0: mkdir -p $DST failed - bailing" && exit 1 )
	echo "$0 Starting rsync to $DST"

    for i in $targets_root
    do
      sudo rsync -av --delete  "$i"  "$DST" || ( echo "$0: rsync failed - bailing" && exit 1 )
    done

    for i in $targets_user_delete
    do
      rsync -av --delete "$HOME/$i" "$DST" || ( echo "$0: rsync failed - bailing" && exit 1 )  
    done

    for i in $targets_user_data
    do
      rsync -av "$i" "$DST" || ( echo "$0: rsync failed - bailing" && exit 1 )  
    done

    for i in $targets_user
    do
      rsync -av "$HOME/$i" "$DST" || ( echo "$0: rsync failed - bailing" && exit 1 )  
    done
else
    echo "$: $DST_root not mounted. please connect the drive"
    exit 1
fi

echo "$0: to $DST successfully completed"
exit 0
