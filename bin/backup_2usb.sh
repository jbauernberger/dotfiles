#!/bin/sh

# backup script, run manually

DST_root=/media/joachim/LEXARcrypt/
DST=$DST_root/`hostname`
ROPTS="-av --chown=joachim:joachim"

if [ -z "`which rsync`" ]; then
	echo "$0: rsync command not on PATH ($PATH)" && exit 1
fi

if [ -d "$DST_root" ]; then
	mkdir -p $DST || ( echo "$0: mkdir -p $DST failed - bailing" && exit 1 )
	echo "$0 Starting rsync to $DST"
    sudo rsync ${ROPTS} --delete /etc $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} --delete ~/.config $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} --delete ~/.offlineimap* $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} --delete ~/.config $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} --delete ~/.cert $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} --delete ~/.ssh $DST || ( echo "$0: rsync failed - bailing" && exit 1 )

    rsync ${ROPTS} ~/bin $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/src/bumblebee-status/ $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} /usr/local/share/xsession/ $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.mutt* $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.gnupg $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.ssh $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.vim* $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.bash* $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.zshrc $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.profile $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    rsync ${ROPTS} ~/.gitconfig $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
else
    echo "$: $DST_root not mounted. please connect the drive"
    exit 1
fi

echo "$0: to $DST successfully completed"
exit 0
