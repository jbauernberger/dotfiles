#!/bin/bash
#
# full system backup script, run manually
# → https://wiki.archlinux.org/index.php/full_system_backup_with_rsync

DST=/media/joachim/Media/$(date +%Y%m%d)/
mkdir -p $DST

if [ -d "$DST" ]; then

    #sudo rsync -aAXv --exclude={"/home","/dev/*","/proc/*","/sys/*","/tmp","/run/*","/mnt","/media","/data","/lost+found"} / $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    sudo rsync -aAXv --exclude={"/lost+found"} /etc $DST || ( echo "$0: rsync failed - bailing" && exit 1 )
    #sudo rsync -aAXv --exclude={"/lost+found"} /usr $DST || ( echo "$0: rsync failed - bailing" && exit 1 )

    sudo rsync -aAXv --delete --exclude={"/home/joachim/Downloads","/home/.ecryptfs","/home/joachim/.adobe","/home/joachim/.bundle","/home/joachim/.cache","/home/joachim/.ccache","/home/joachim/.cargo","/home/joachim/.cgvg","/home/joachim/.choosenim","/home/joachim/.config/Signal","/home/joachim/.config/google-chrome","/home/joachim/.config/skypeforelinux","/home/joachim/.dotfiles","/home/joachim/.local/share","/home/joachim/.mozilla","/home/joachim/.Mail","/home/joachim/.npm*","/home/joachim/.racket","/home/joachim/.subversion","/home/joachim/.thumbnails","/home/joachim/.thunderbird","/home/joachim/hawaii-edu_freeman","/home/joachim/log","/home/joachim/Shots"} /home $DST || ( echo "$0: rsync failed - bailing" && exit 1 )

else
    echo "$: $DST not mounted. please connect the drive"
    exit 1
fi

echo "$0: to $DST successfully completed"
exit 0
