#!/bin/bash

BATTINFO=`acpi -b | grep Discharging`
if [ -z "$BATTINFO"  ]; then
    exit 0 # on AC
fi

PERCENT_LEFT=`echo "$BATTINFO" | cut -f 4 -d " " |sed -re 's/%,//g'`
if [ -z "$PERCENT_LEFT"  ]; then
    echo "BATTINFO regex fail - unexpected pattern: $BATTINFO"
    exit 1 # parse error
fi

if [[ $PERCENT_LEFT -lt 8 ]]; then
    $HOME/bin/timer.sh 1 "I have less than $PERCENT_LEFT %  battery ya ignorant Basterds." 100
elif [[ $PERCENT_LEFT -lt 12 ]]; then
    $HOME/bin/timer.sh 1 "My Latop Battery is less than $PERCENT_LEFT % yer mad Bastards. Anyone please fecking do something or we are all gonna Die." 80
elif [[ $PERCENT_LEFT -lt 15 ]]; then
    $HOME/bin/timer.sh 1 "Hey $USER, urgent reminder the Latop Battery is Dead in less than $PERCENT_LEFT %" 60
elif [[ $PERCENT_LEFT -lt 20 ]]; then
    $HOME/bin/timer.sh 1 "Gentle Reminder. Your Latop Battery charge is less than $PERCENT_LEFT %" 20
fi
