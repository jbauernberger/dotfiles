#!/bin/bash
# Prevent Marshall Stanmore III from auto suspend
# this is triggered by systemd-timer .config/systemd/user/bt-disable-sleep.timer
# See: https://www.reddit.com/r/linuxaudio/comments/1bjkezq/prevent_bluetooth_speaker_going_to_standby_by/

declare -r mac="68:59:32:DE:A9:32"
#declare -r me="${0##*/}"

# check if media is playing
if [ "$(/usr/bin/playerctl status)" == "Playing" ]; then
	exit 0
else
	# check if $mac is connected. 
	/usr/bin/hcitool con|grep "$mac" &> /dev/null && \
	echo "sending whitenoise to $mac" && \
	/usr/bin/play -n -c1 synth 3 whitenoise band -n 1 1 fade h 1 3 1 gain -10000 &> /dev/null
fi

exit 0
