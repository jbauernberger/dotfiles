#!/bin/bash
## concat 2 videos with different aspect ratios.
## see https://stackoverflow.com/questions/52830468/ffmpeg-concat-videos-error-do-not-match-the-corresponding-output-link
#

IN_1=$1
IN_2=$2

OUT=~/Downloads/output.mp4

ffmpeg -i "$IN_1" -i "$IN_2" \
  -filter_complex "[0:v:0] [0:a:0] [1:v:0] [1:a:0] concat=n=2:v=1:a=1 [v] [a]" \
  -map "[v]" -map "[a]" "$OUT"



#ffmpeg -i "$IN_1" -i "$IN_2" \
#        -f lavfi -t 0.1 -i anullsrc -filter_complex \
#        "[0:v]scale=640:480:force_original_aspect_ratio=decrease,pad=640:480:(ow-iw)/2:(oh-ih)/2,setsar=1[v0]; [1:v]setsar=1[v1];[v0][0:a][v1][1:a]concat=n=2:v=1:a=1[outv][outa]" -map "[outv]" -map "[outa]" -vcodec libx264 -crf 27 -preset ultrafast -threads 2 "$OUT"

## not working:
#ffmpeg -i new1.mp4 -i new2.mp4 -i new3.mp4 -i new4.mp4 -filter_complex \
#        "[0]setdar=16/9[a];[1]setdar=16/9[b];[2]setdar=16/9[c];[3]setdar=16/9[d]; \
#         [a][b][c][d]concat=n=4:v=1:a=1" output.mp4
