#!/bin/bash

# see supported ranges with: cpupower frequency-info
sudo cpupower frequency-set -g powersave
sudo cpupower frequency-set -d 400MHz
sudo cpupower frequency-set -u 1.7GHz

cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_*
