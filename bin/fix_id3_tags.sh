#!/bin/bash

echo "Warning: ensure input file list does NOT contain unescaped special shell characters"

sleep 5

rm -f ./t-tmp.sh # clean up from prev runs

for i in *.mp3
do
  echo mid3v2 -a \"Kendrick Lamar\" -A \"Unreleased\" "`echo \"$i\" | sed -re 's/_/\ /g' | sed -re 's/^(.*)\.mp3$/\-e\ \-t\ \"\1\"/g'`" "./$i" >> ./t-tmp.sh

done

echo "generated command file ./t-tmp-.sh"
echo "now run bash ./t-tmp.sh to apply the id3 tags with mid3v2"
