#!/bin/bash
#
#
# https://askubuntu.com/questions/113544/how-can-i-reduce-the-file-size-of-a-scanned-pdf-file
#
#

GS_PATH=`command -v gs`

INPUT_PDF=$1

if [[ -z $INPUT_PDF ]]; then
	echo "$0: missing arguments: expected path to input pdf"
	exit 1	
fi
if [[ -e $INPUT_PDF ]]; then

	$GS_PATH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output_screen_$INPUT_PDF $INPUT_PDF 
	$GS_PATH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output_ebook_$INPUT_PDF $INPUT_PDF 
	$GS_PATH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output_prepress_$INPUT_PDF $INPUT_PDF 
	$GS_PATH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output_printer_$INPUT_PDF $INPUT_PDF 
	$GS_PATH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output_default_$INPUT_PDF $INPUT_PDF 

else
	echo "$0: unable to access $INPUT_PDF"
	exit 1
fi

exit 0
