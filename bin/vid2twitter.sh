#!/bin/sh

IN="$1"
OUT="$2"

ffmpeg -i "$IN" -pix_fmt yuv420p -vcodec libx264 -vf scale=640:-1 -acodec aac -vb 1024k -minrate 1024k -maxrate 1024k -bufsize 1024k -ar 44100  -ac 2  -strict experimental -r 30 "$OUT"

