#!/bin/bash

total=0
for i in `cat youtube-dl.log|grep 'download] 100% of' |cut -d " " -f4|sed -re 's/MiB//'`
do
  total=`awk "BEGIN {print $total+$i; exit}"`
  echo $total
done

echo "youtube-dl fetched a total of $total MiB"
